using Android.App;
using Android.OS;
using EasyBike.Models;
using System.Collections.Generic;
using Android.Widget;
using EasyBike.Droid.Helpers;
using Android.Views;
using Android.Graphics;
using GalaSoft.MvvmLight.Helpers;

namespace EasyBike.Droid.Views
{
    [Activity(Label = "ContractsActivity")]
    public partial class ContractsActivity 
    {
     
        private List<Country> countries;

        protected override async void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Contracts);

            var test = FindViewById<ExpandableListView>(Resource.Id.ContractsList);
        
            //await Task.Delay(30);
            //var t = Task.Run(async () =>
            //{
            //    vm = SimpleIoc.Default.GetInstanceWithoutCaching<ContractsViewModel>();
            //   // countries = await vm.GetCountries().ConfigureAwait(false);
            //};
            countries = await ViewModel.GetCountries();

            ContractsList.SetAdapter(new CountryListAdapter(this, countries));
            ContractsList.ChildClick += ContractsList_ChildClick;

           // ContractsList.Adapter =  countries.GetAdapter(GetContractAdapter);
        }


        private void ContractsList_ChildClick(object sender, ExpandableListView.ChildClickEventArgs e)
        {
            var contract = countries[e.GroupPosition].Contracts[e.ChildPosition];
            ViewModel.ContractTappedCommand.Execute(contract);
            //(ContractsList.Adapter as CountryListAdapter).GetChildView(e.GroupPosition)
            e.ClickedView.SetBackgroundColor(Color.ParseColor("#676767"));
            //_countries[groupPosition].Contracts[childPosition].

            var checkBox = e.ClickedView.FindViewById<CheckBox>(Resource.Id.ContractCheckBox);

            //checkBox.SetBinding(
            //     () => contract.Downloaded,
            //     () => checkBox.Checked);

        }


        //private View GetContractAdapter(int position, Contract contract, View convertView, View view2)
        //{
        //    // Not reusing views here
        //    convertView = LayoutInflater.Inflate(Resource.Layout.FlowerTemplate, null);

        //    var title = convertView.FindViewById<TextView>(Resource.Id.NameTextView);
        //    title.Text = contract.Name;

        //    //var image = convertView.FindViewById<ImageView>(Resource.Id.FlowerImageView);
        //    // ImageDownloader.AssignImageAsync(image, flower.Model.Image, this);

        //    return convertView;
        //}
    }
}